Please describe the following to make your issue clear and actionable for our developers, so they are more likely to address it.

# What version of tattler are you reporting this issue for?

Find this out with e.g. `pip freeze | grep tattler`.


# What is the behavior you have issue with?

Describe the behavior in a way that can be reproduced by the reader.


# What behavior would you expect instead?

Describe how the expected behavior would differ from the actual behavior.


# What is the impact of the issue?

Describe what concrete problem this issue is causing. Does this prevent notifications from arriving? Does it reduce downtime?


# How severe is this impact (low, medium, high)?

**Low**: an annoyance that can be tolerated.

**Medium**: an annoyance that occasionally requires manual intervention.

**High**: An issue making tattler unsuited for use in a production environment.
